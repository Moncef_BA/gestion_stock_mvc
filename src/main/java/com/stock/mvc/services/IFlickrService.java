package com.stock.mvc.services;

import java.io.InputStream;

public interface IFlickrService {
	public String savePhoto(InputStream strem, String fileName) throws Exception ;
}
